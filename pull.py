import xmltodict
import requests
from model.canadate import Canadate
from model.contest import Contest
import json
import urllib3

def pullAK():
    canadates = []
    link = "https://www.elections.alaska.gov/results/20GENR/data/sovc/ElectionSummaryReportRPT8.xml"

    session = requests.session()
    r = session.get(link, verify=False)
    session.close()
    info = xmltodict.parse(r.text)
    contest_list = info['Report']['tabBatchIdList']['TabBatchGroup_Collection']['TabBatchGroup']['ElectionSummarySubReport']['Report']['contestList']['ContestIdGroup_Collection']['ContestIdGroup']

    for contestgp in contest_list:
        if 'U.S. Senator' in contestgp['@contestId']:

            new_contest = Contest(contestgp['@contestId'])
            for info in contestgp['ContestStatistics']['Report']['Tablix1']['Textbox7']['cgGroup_Collection']['cgGroup']:

                if 'Election Day' in info['cgId2']['@cgId2']:
                    new_contest.in_persion = info['cgId2']['@ballotsTextBox']
                elif 'Absentee' in info['cgId2']['@cgId2']:
                    new_contest.absentee = info['cgId2']['@ballotsTextBox']
                elif 'Early Voting' in info['cgId2']['@cgId2']:
                    new_contest.early_vote = info['cgId2']['@ballotsTextBox']
                elif '@countingGroupName' in info['cgId2']['@cgId2']:
                    new_contest.question = info['cgId2']['@ballotsTextBox']
                elif 'Remote' in info['cgId2']['@cgId2']:
                    new_contest.remote = info['cgId2']['@ballotsTextBox']
                    
            for contest in contestgp['CandidateResults']['Report']['Tablix1']['chGroup_Collection']['chGroup']:
                new_canadate = Canadate(contest['candidateNameTextBox4']['@candidateNameTextBox4'],contest['candidateNameTextBox4']['Textbox2']['@Textbox14'])
                for info in contest['candidateNameTextBox4']['cgGroup_Collection']['cgGroup']:
                    if 'Election Day' in info['@countingGroupName']:
                        new_canadate.in_persion  = info['@vot7']
                    elif 'Absentee' in info['@countingGroupName']:
                        new_canadate.absentee = info['@vot7']
                    elif 'Early Voting' in info['@countingGroupName']:
                        new_canadate.early_vote = info['@vot7']
                    elif 'Question' in info['@countingGroupName']:
                        new_canadate.question = info['@vot7']
                    elif 'Remote' in info['@countingGroupName']:
                        new_canadate.remote = info['@vot7']

                canadates.append(new_canadate)
                
            print('Done')

    

if __name__ == "__main__":
    pullAK()